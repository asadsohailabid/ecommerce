<?php

include 'includes/db.php';


session_start();

// $sel_products = "SELECT * FROM models";
// $run_products_sql = mysqli_query($conn, $sel_products);
// $total_products = mysqli_num_rows($run_products_sql);

if (isset($_SESSION['cart'])) {
    $total_products = sizeof($_SESSION['cart']);
} else {
    $total_products = 0;
}

if (isset($_GET['storeRes'])) {
    if ($_GET['storeRes'] == "failure")
        $storeResult = '<div class="alert alert-danger">Order Details could not be stores</div>';
} else {
    $storeResult = '';
}

// print_r($_SESSION['cart'][0]);

if (isset($_SESSION['cart'])) {
    $productData = $_SESSION['cart'];
} else {
    $productData = [' '];
}


?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Cart</title>

    <!-- JQUERY LINKING HERE -->
    <script src="https://code.jquery.com/jquery-3.5.1.js" integrity="sha256-QWo7LDvxbWT2tbbQ97B53yJnYU3WhH/C8ycbRAkjPDc=" crossorigin="anonymous"></script>

    <!-- BOOTSTRAP CSS LINKING HERE -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">

    <!-- BOOTSTRAP JS LINKING HERE -->
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js" integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" crossorigin="anonymous"></script>

    <script>
        $(document).ready(function() {

            var productsArray = <?php echo json_encode($productData); ?>;

            //function to add and validate if all the quantity fields have been filled or not
            function add_quantity_to_products() {
                var valid = true;

                $('.quantity').each(function(i, obj) {
                    var parentId = $(this).parent()[0].id;
                    var value = $(this).val();
                    if (value == '') {
                        valid = false;
                    }
                });

                if (valid) {
                    $('.quantity').each(function(i, obj) {
                        var parentId = obj.parentElement.id;
                        var value = $(obj).val();
                        var productIndex = -1;
                        productsArray.forEach(function(item, index) {
                            if (parseInt(item[0]) == parentId) productIndex = index;
                        });

                        if (productIndex == -1) console.log('Not Found');
                        else {
                            if (productsArray[productIndex].length == 2) {
                                productsArray[productIndex].push(value);
                            } else {
                                productsArray[productIndex][2] = value;
                            }
                        }
                    });
                } else {
                    alert('Please Enter Some Value in the field');
                }

                return valid;
            }

            //function to calculate sub_total and net_total values
            $(document).mousemove(function() {
                var isValid = true;

                $('.quantity').each(function(i, obj) {
                    var value = $(this).val();
                    if (value == '') {
                        isValid = false;
                    }
                });

                if (isValid) {

                    //change the value of sub_total and this net_total
                    var sub_total = 0;
                    var quantity = 0;

                    //check the total quantity
                    $('.quantity').each(function(i, obj) {
                        quantity += parseInt($(this).val());
                    });

                    //check the value of sub_total
                    sub_total = quantity*7;

                    var net_total = sub_total+3;

                    
                    document.getElementById("sub_total").innerHTML = sub_total;
                    document.getElementById("net_total").innerHTML = net_total;

                    // console.log(sub_total);
                    // console.log(net_total);

                }

            });

            //create function to delete an item from array
            $(".deleteBtn").click(function() {
                //check element's parent id
                var parentId = $(this).parent()[0].id;

                //check if the parent element exists in the products array
                var productIndex = -1;
                productsArray.forEach(function(item, index) {
                    if (parseInt(item[0]) == parentId) productIndex = index;
                });

                if (productIndex == -1) console.log('No Such Element Exists');
                else {
                    if (productsArray[productIndex]) {
                        //delete the element from the products array
                        productsArray.splice(productIndex, productIndex + 1);
                    } else {
                        console.log('No Such Element Exists');
                    }
                }
                //remove the parent element using jquery
                $(this).parent().parent().remove();

                //remove entry from session
                $.ajax({
                    url: 'includes/delete_session_item.php',
                    method: 'post',
                    data: {
                        id: parentId
                    },
                    success: function(response) {
                        if (response == "SUCCESS") {
                            alert("Item Deleted Successfully!");
                        } else if (response == "FAILURE") {
                            alert("Serve Not Responding");
                        }
                    }
                });
            });

            $("#buyBtn").click(function() {
                var valid = add_quantity_to_products();

                if (valid) {

                    $("#buyModal").modal();

                    $("#submitForm").click(function() {
                        var nameVal = $("#name").val();
                        var emailVal = $("#email").val();
                        var contactVal = $("#contact").val();
                        var addressVal = $("#address").val();
                        var postal_codeVal = $("#postal_code").val();
                        var cityVal = $("#city").val();
                        var valid = true;

                        if (nameVal == '' || emailVal == '' || contactVal == '' || addressVal == '' || postal_codeVal == '') {
                            valid = false;
                        }

                        //other validations here

                        if (valid) {
                            var customerData = {
                                'name': nameVal,
                                'email': emailVal,
                                'contact': contactVal,
                                'address': addressVal,
                                'postal_code': postal_codeVal,
                                'city': cityVal,
                            };
                        }

                        if (valid) {
                            //send ajax request here
                            $.ajax({
                                url: "includes/store_order_details.php",
                                method: "post",
                                data: {
                                    products: productsArray,
                                    customer: customerData,
                                    flag: true
                                },
                                success: function(response) {
                                    if (response == "SUCCESS") {
                                        console.log('Sucesssss');
                                        window.location.replace('../index.php?storeRes=success');
                                    } else if (response == "FAILURE") {
                                        console.log('Failureeeee');
                                        window.location.replace('../cart.php?storeRes=failure');
                                    }
                                }
                            });
                        } else {
                            //show alert here
                            alert('Please Fill All the required fields');
                        }
                    });

                } else {
                    console.log('Something went wrong');
                }
            });
        });
    </script>

</head>

<body>
    <div class="modal fade" id="buyModal" role="dialog">
        <div class="modal-dialog modal-dialog-centered">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <h3 class="text-center">Enter Personal Details</h3>
                </div>
                <div class="modal-body">
                    <form class="form-horizontal" action="" role="form" method="post">
                        <div class="form-group row">
                            <label class="col-md-1"></label>
                            <label class="col-md-2">Name</label>
                            <input type="text" class="form-control col-md-8" name="name" id="name" placeholder="Your name">
                        </div>
                        <div class="form-group row">
                            <label class="col-md-1"></label>
                            <label class="col-md-2">Email</label>
                            <input type="email" class="form-control col-md-8" name="email" id="email" placeholder="Your email">
                        </div>
                        <div class="form-group row">
                            <label class="col-md-1"></label>
                            <label class="col-md-2">Contact</label>
                            <input type="text" class="form-control col-md-8" name="contact" id="contact" placeholder="Your Phone No">
                        </div>
                        <div class="form-group row">
                            <label class="col-md-1"></label>
                            <label class="col-md-2">Address</label>
                            <textarea class="form-control col-md-8" style="resize: none;" placeholder="Your Address" name="address" id="address"></textarea>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-3 text-right">Postal Code</label>
                            <input type="number" class="form-control col-md-8" name="postal_code" id="postal_code" placeholder="Your Postal Code">
                        </div>
                        <div class="form-group row">
                            <label class="col-md-3 text-right">City</label>
                            <input type="text" class="form-control col-md-8" name="city" id="city" placeholder="Your City">
                        </div>
                        <div class="form-group text-right row">
                            <label class="col-md-11">
                                <input type="button" class="btn btn-danger" value="Proceed" name="submitForm" id="submitForm">
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <!-- NAVIGATION BAR HERE -->
    <?php include 'includes/header.php'; ?>

    <div class="container">
        <?php echo $storeResult; ?>
        <div class="row">
            <div class="col-md-8">
                <div id="left_section">
                    <?php
                    if (!isset($_SESSION['cart'])) {
                        echo '<div class="alert alert-danger">Cart Empty</div>';
                    } else { ?>
                        <div class="card">
                            <div class="card-header bg-white">
                                <h3>Shopping Cart (<?php echo $total_products; ?>)</h3>
                            </div>
                            <div class="card-body">
                                <div class="table-responsive">
                                    <table class="table">
                                        <thead>
                                            <th>#</th>
                                            <th>Image</th>
                                            <th>Details</th>
                                            <th>Price</th>
                                            <th>Quantity</th>
                                            <th></th>
                                        </thead>
                                        <tbody>
                                            <?php
                                            $i = 0;
                                            foreach ($_SESSION['cart'] as $cartItem) {
                                                $sel_products = "SELECT * FROM models WHERE m_id= '$cartItem[0]' ";
                                                $run_products_sql = mysqli_query($conn, $sel_products);

                                                while ($model = mysqli_fetch_assoc($run_products_sql)) {
                                                    $sel_cat = "SELECT * FROM categories WHERE cat_id = '$model[cat_id]'";
                                                    $run_cat_sql = mysqli_query($conn, $sel_cat);

                                                    while ($category = mysqli_fetch_assoc($run_cat_sql)) $cat_name = $category['cat_name'];

                                                    $color = $cartItem[1];
                                                    switch ($color) {
                                                        case "red":
                                                            $imgAddress = "images/mobile/redThumbnail.jpg";
                                                            break;
                                                        case "black":
                                                            $imgAddress = "images/mobile/blackThumbnail.jpg";
                                                            break;
                                                        case "blue":
                                                            $imgAddress = "images/mobile/blueThumbnail.jpg";
                                                            break;
                                                        case "gold":
                                                            $imgAddress = "images/mobile/goldThumbnail.jpg";
                                                            break;
                                                        case "purple":
                                                            $imgAddress = "images/mobile/purpleThumbnail.jpg";
                                                            break;
                                                        case "silver":
                                                            $imgAddress = "images/mobile/silverThumbnail.jpg";
                                                            break;
                                                        default:
                                                            $imgAddress = "images/mobile/silverThumbnail.jpg";
                                                            break;
                                                    }

                                            ?>
                                                    <tr>
                                                        <td><?php echo ++$i; ?></td>
                                                        <td>
                                                            <img src="<?php echo $imgAddress ?>" width="100%">
                                                        </td>
                                                        <td>
                                                            <h5><?php echo $cat_name . ' ' . $model['m_name'] . ' (' . strtoupper($cartItem[1]) . ')'; ?><h5>
                                                        </td>
                                                        <td>
                                                            € 7.00
                                                        </td>
                                                        <td id="<?php echo $model['m_id']; ?>">
                                                            <input class="form-control quantity" type="number" name="quantity" id="quantity" />
                                                        </td>
                                                        <td id="<?php echo $model['m_id']; ?>">
                                                            <button type="button" class="close deleteBtn" aria-label="Delete">
                                                                <span aria-hidden="true">&times;</span>
                                                            </button>
                                                        </td>
                                                    </tr>
                                            <?php
                                                }
                                            }
                                            ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    <?php
                    }
                    ?>
                </div>
            </div>
            <div class="col-md-4" id="right_section">
                <?php
                if (!isset($_SESSION['cart'])) {
                } else { ?>
                    <div class="card">
                        <div class="card-header bg-white">
                            <h4>Order Summary</h4>
                        </div>
                        <div class="card-body">
                            <div id="billing_section">
                                <div class="form-group row">
                                    <label class="col-md-6">Sub Total:</label>
                                    <label id="sub_total" class="col-md-6 text-right">€ <?php echo $total_products * 7; ?></label>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-6">Shipping:</label>
                                    <label class="col-md-6 text-right">€ 3.00</label>
                                </div>
                                <hr />
                                <div class="form-group row">
                                    <h5 class="col-md-6">Net Total:</h5>
                                    <h3 id="net_total" class="col-md-6 text-right">€ <?php echo ($total_products * 7) + 3; ?></h3>
                                </div>
                                <div class="form-group row">
                                    <input id="buyBtn" type="button" class="btn btn-danger btn-block btn-lg" value="BUY NOW">
                                </div>
                            </div>
                        </div>
                    </div>
                <?php } ?>
            </div>
        </div>
    </div>
</body>

</html>