<?php

session_start();
if (!isset($_SESSION['user_email'])) {
    header('Location: ../index.php');
}

include 'includes/db.php';

if (isset($_GET['order_id'])) {
    $order_id = $_GET['order_id'];

    $sel_order = "SELECT * FROM orders WHERE order_id = '$order_id'";

    if (!($run_order_sql = mysqli_query($conn, $sel_order))) {
        header('Location: orders.php?viewRes=not_found');
    } else {
        while ($order = mysqli_fetch_assoc($run_order_sql)) {
            $customer_name = $order['cust_name'];
            $customer_email = $order['cust_email'];
            $customer_contact = $order['cust_contact'];
            $customer_address = $order['cust_address'];
            $customer_postal_code = $order['cust_postal_code'];
            $customer_city = $order['cust_city'];
            $order_status = $order['status'];
        }

        $sel_products = "SELECT * FROM ordered_products WHERE order_id = '$order_id'";
        $sub_total = 0;

        if ($run_products = mysqli_query($conn, $sel_products)) {
            while ($product = mysqli_fetch_assoc($run_products)) {
                $sub_total += (7 * $product['quantity']);
            }
        }

        $shipping = 3;
        $net_total = $sub_total + $shipping;
    }
} else {
    header('Location: orders.php');
}

?>


<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>View Order</title>
    <!-- JQUERY LINKING HERE -->
    <script src="https://code.jquery.com/jquery-3.5.1.js" integrity="sha256-QWo7LDvxbWT2tbbQ97B53yJnYU3WhH/C8ycbRAkjPDc=" crossorigin="anonymous"></script>

    <!-- BOOTSTRAP CSS LINKING HERE -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">

    <!-- BOOTSTRAP JS LINKING HERE -->
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js" integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" crossorigin="anonymous"></script>


</head>

<body>
    <?php include 'includes/header.php'; ?>
    <div class="container my-5">
        <!-- UPPER SECTION -->
        <div class="my-2" style="display:flex;">
            <div class="card col-md-7 mr-4 border border-info shadow" style="width: 100%;justify-self:flex-start;">
                <div class="card-header bg-white">
                    <h4>Customer Details</h4>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-sm">
                            <tr>
                                <th>Name</th>
                                <td><?php echo $customer_name; ?></td>
                            </tr>
                            <tr>
                                <th>Email</th>
                                <td><?php echo $customer_email; ?></td>
                            </tr>
                            <tr>
                                <th>Contact</th>
                                <td><?php echo $customer_contact; ?></td>
                            </tr>
                            <tr>
                                <th>Address</th>
                                <td><?php echo $customer_address; ?></td>
                            </tr>
                            <tr>
                                <th>Postal Code</th>
                                <td><?php echo $customer_postal_code; ?></td>
                            </tr>
                            <tr>
                                <th>City</th>
                                <td><?php echo $customer_city; ?></td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
            <div class="card col-md-4 ml-4 border border-info shadow" style="width: 100%;align-self:center;">
                <div class="card-header bg-white">
                    <h4>Billing Details</h4>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table">
                            <tr>
                                <th>Sub Total: </th>
                                <td class="text-right">€ <?php echo $sub_total; ?></td>
                            </tr>
                            <tr>
                                <th>Shipping: </th>
                                <td class="text-right">€ <?php echo $shipping; ?></td>
                            </tr>
                            <tr>
                                <th>Net Total: </th>
                                <td class="text-right">€ <?php echo $net_total; ?></td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <!-- LOWER SECTION -->
        <div class="my-2" style="display:flex;justify-content:center;">
            <div class="card border border-info shadow" style="width: 100%;">
                <div class="card-header bg-white">
                    <h4>Products</h4>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Category</th>
                                    <th>Model Name</th>
                                    <th>Color</th>
                                    <th>Quantity</th>
                                    <th>Price</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php

                                $sel_products = "SELECT * FROM ordered_products WHERE order_id = '$order_id'";
                                $count = 1;

                                if ($run_products = mysqli_query($conn, $sel_products)) {

                                    while ($product = mysqli_fetch_assoc($run_products)) {

                                        $cat_name = '';
                                        $model_name = '';
                                        $color = $product['color'];
                                        $quantity = $product['quantity'];
                                        $price = 7;

                                        $sel_model = "SELECT * FROM models WHERE m_id = '$product[model_id]'";
                                        $run_model_sql = mysqli_query($conn, $sel_model);

                                        while ($model = mysqli_fetch_assoc($run_model_sql)) {
                                            $cat_id = $model['cat_id'];
                                            $model_name = $model['m_name'];
                                        }

                                        $sel_cat = "SELECT * FROM categories WHERE cat_id = '$cat_id'";
                                        $run_cat_sql = mysqli_query($conn, $sel_cat);

                                        while ($category = mysqli_fetch_assoc($run_cat_sql)) {
                                            $cat_name = $category['cat_name'];
                                        }

                                        echo '<tr>';
                                        echo '<td>' . $count++ . '</td>';
                                        echo '<td>' . $cat_name . '</td>';
                                        echo '<td>' . $model_name . '</td>';
                                        echo '<td>' . ucfirst($color) . '</td>';
                                        echo '<td>' . $quantity . '</td>';
                                        echo '<td>€ ' . $price . '</td>';
                                        echo '</tr>';
                                    }
                                } else {
                                    echo '<div class="alert alert-danger">No Products Available</div>';
                                }

                                ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

        <!-- BUTTON SECTION -->
        <div class="my-5" style="display:flex;justify-content:center;">
            <a href="orders.php" class="btn btn-info mx-5 col-md-3">Go Back</a>
        </div>
    </div>
</body>

</html>