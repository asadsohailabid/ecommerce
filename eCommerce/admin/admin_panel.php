<?php


session_start();

if (!isset($_SESSION['user_email'])) {
    header('Location: ../index.php');
}


?>


<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Admin Panel</title>

    <!-- JQUERY LINKING HERE -->
    <script src="https://code.jquery.com/jquery-3.5.1.js" integrity="sha256-QWo7LDvxbWT2tbbQ97B53yJnYU3WhH/C8ycbRAkjPDc=" crossorigin="anonymous"></script>

    <!-- BOOTSTRAP CSS LINKING HERE -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">

    <!-- BOOTSTRAP JS LINKING HERE -->
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js" integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" crossorigin="anonymous"></script>


</head>

<body>
    <?php include 'includes/header.php'; ?>
    <div class="container my-3 row text-center" style="margin-left:auto; margin-right:auto;">
        <div class="col-md-6 my-3">
            <div class="card border border-info shadow">
                <div class="card-header bg-white border">
                    <h3>Models</h3>
                </div>
                <div class="card-body" style="display: flex;">
                    <a href="models.php" class="btn btn-info mx-2">View Models</a>
                    <a href="add_model.php" class="btn border border-dark mx-2">Add Model</a>
                </div>
            </div>
        </div>
        <div class="col-md-6 my-3">
            <div class="card border border-info shadow">
                <div class="card-header bg-white border">
                    <h3>Orders</h3>
                </div>
                <div class="card-body">
                    <a href="orders.php" class="btn btn-info mx-2">View Orders</a>
                </div>
            </div>
        </div>
    </div>
</body>

</html>