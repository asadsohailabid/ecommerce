<?php

include 'includes/db.php';

session_start();
if (!isset($_SESSION['user_email'])) {
    header('Location: ../index.php');
}


if (isset($_GET['edit_id'])) {
    $model_id = $_GET['edit_id'];

    $sel_model = "SELECT * FROM models WHERE m_id = '$model_id'";
    if (!($run_model_sql = mysqli_query($conn, $sel_model))) {
        header('Location: models.php?editRes=not_found');
    }
    while ($model = mysqli_fetch_assoc($run_model_sql)) {
        $model_name = $model['m_name'];
        $model_cat = $model['cat_id'];
    }
}

if (isset($_POST['submit'])) {

    $new_name = $_POST['model_name'];
    $id = $_GET['model_id'];
    $cat_id = $_POST['cat_id'];

    $update_sql = "UPDATE models SET m_name = '$new_name', cat_id = '$cat_id' WHERE m_id = '$id'";

    if ($run_update = mysqli_query($conn, $update_sql)) {
        header('Location: models.php?editRes=success');
    } else {
        header('Location: models.php?editRes=failure');
    }
}



?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Edit Model</title>


    <!-- JQUERY LINKING HERE -->
    <script src="https://code.jquery.com/jquery-3.5.1.js" integrity="sha256-QWo7LDvxbWT2tbbQ97B53yJnYU3WhH/C8ycbRAkjPDc=" crossorigin="anonymous"></script>

    <!-- BOOTSTRAP CSS LINKING HERE -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">

    <!-- BOOTSTRAP JS LINKING HERE -->
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js" integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" crossorigin="anonymous"></script>

</head>

<body>
    <?php include 'includes/header.php'; ?>
    <div class="container mt-4">
        <div class="card border border-info shadow-lg">
            <div class="card-header bg-white">
                <h2>Edit Model</h2>
            </div>
            <div class="card-body">
                <form action="edit_model.php?model_id=<?php echo $model_id; ?>" role="form" method="POST" class="form-horizontal">
                    <div class="form-group row">
                        <label class="col-md-3 text-center" for="cat_id">Category</label>
                        <select id="cat_id" name="cat_id" class="form-control col-md-8" required>
                            <option value="">Select Category</option>
                            <?php
                            $sel_cat = "SELECT * FROM categories";
                            $run_cat_sql = mysqli_query($conn, $sel_cat);
                            while ($category = mysqli_fetch_assoc($run_cat_sql)) {
                                if ($model_cat == $category['cat_id']) echo '<option value="' . $category['cat_id'] . '" selected>';
                                else echo '<option value="' . $category['cat_id'] . '">';
                                echo $category['cat_name'];
                                echo '</option>';
                            }
                            ?>
                        </select>
                    </div>
                    <div class="form-group row">
                        <label class="col-md-3 text-center" for="model_name">Model Name</label>
                        <input id="model_name" name="model_name" class="form-control col-md-8" value="<?php echo $model_name; ?>" required />
                    </div>
                    <div>
                        <label class="col-md-3 text-center" for="submit"></label>
                        <button type="submit" id="submit" name="submit" class="btn btn-info btn-lg">Submit</button>
                    </div>
                </form>
            </div>
        </div>

    </div>
</body>

</html>