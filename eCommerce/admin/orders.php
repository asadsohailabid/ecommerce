<?php


session_start();
if (!isset($_SESSION['user_email'])) {
    header('Location: ../index.php');
}

include 'includes/db.php';

//code for changing the status of the order
if (isset($_GET['mark_id'])) {
    $mark_id = $_GET['mark_id'];

    $sel_order = "SELECT * FROM orders WHERE order_id = '$mark_id'";
    $run_order_sql = mysqli_query($conn, $sel_order);

    while ($order = mysqli_fetch_assoc($run_order_sql)) {
        if ($order['status'] == 0) $status = 1;
        else if ($order['status'] == 1) $status = 0;
        $update_status = "UPDATE orders SET status = '$status'";
        if ($run_update = mysqli_query($conn, $update_status)) {
            header('Location: orders.php?markRes=success');
        } else {
            header('Location: orders.php?markRes=failure');
        }
    }
}
if (isset($_GET['markRes'])) {
    if ($_GET['markRes'] == 'success') {
        $markResult = '<div class="alert alert-info">Order has been updated!</div>';
    } else if ($_GET['markRes'] == 'failure') {
        $markResult = '<div class="alert alert-danger">Order could not be updated!</div>';
    }
} else {
    $markResult = '';
}

//code for deleting the order
if (isset($_GET['del_id'])) {
    $del_id = $_GET['del_id'];

    $sel_order = "DELETE FROM orders WHERE order_id = '$del_id'";
    if ($run_order_sql = mysqli_query($conn, $sel_order)) {
        header('Location: orders.php?delRes=success');
    } else {
        header('Location: orders.php?delRes=failure');
    }
}
if (isset($_GET['delRes'])) {
    if ($_GET['delRes'] == 'success') {
        $delResult = '<div class="alert alert-info">Order has been deleted!</div>';
    } else if ($_GET['delRes'] == 'failure') {
        $delResult = '<div class="alert alert-danger">Order could not be deleted!</div>';
    }
} else {
    $delResult = '';
}

?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Orders</title>

    <!-- JQUERY LINKING HERE -->
    <script src="https://code.jquery.com/jquery-3.5.1.js" integrity="sha256-QWo7LDvxbWT2tbbQ97B53yJnYU3WhH/C8ycbRAkjPDc=" crossorigin="anonymous"></script>

    <!-- BOOTSTRAP CSS LINKING HERE -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">

    <!-- BOOTSTRAP JS LINKING HERE -->
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js" integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" crossorigin="anonymous"></script>


</head>

<body>
    <?php include 'includes/header.php'; ?>
    <!-- BUTTON SECTION -->
    <div class="my-3" style="display:flex;justify-content:flex-end;width:90%;">
        <a href="admin_panel.php" class="btn btn-info mx-5 col-md-3 shadow-lg">Go Back</a>
    </div>
    <div class="container my-3">
        <div><?php echo $markResult; ?></div>
        <div><?php echo $delResult; ?></div>
        <div class="card border border-info  shadow-lg">
            <div class="card-header bg-white">
                <h2>Orders</h2>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Customer Name</th>
                                <th>Customer Email</th>
                                <th>Customer Address</th>
                                <th>Status</th>
                                <th colspan="2">Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php

                            $sel_orders = "SELECT * FROM orders";
                            $run_orders_sql = mysqli_query($conn, $sel_orders);
                            $count = 1;


                            while ($order = mysqli_fetch_assoc($run_orders_sql)) {
                                if ($order['status'] == 0) {
                                    $status = 'Pending';
                                } else if ($order['status'] == 1) {
                                    $status = 'Done';
                                } else {
                                    $status = 'Unknown';
                                }

                                echo '
                                    <tr>
                                        <td>' . $count++ . '</td>
                                        <td>' . $order['cust_name'] . '</td>
                                        <td>' . $order['cust_email'] . '</td>
                                        <td>' . $order['cust_address'] . '</td>
                                        <td>' . $status . '</td>
                                        <td><a href="view_order.php?order_id=' . $order['order_id'] . '" class="btn btn-info btn-sm">View</a></td>
                                ';
                                if ($order['status'] == 0) {
                                    echo '<td><a href="orders.php?mark_id=' . $order['order_id'] . '" class="btn btn-primary btn-sm">Mark As Done</a></td>';
                                } else if ($order['status'] == 1) {
                                    echo '<td><a href="orders.php?mark_id=' . $order['order_id'] . '" class="btn btn-primary btn-sm">Mark As Pending</a></td>';
                                }
                                echo '<td><a href="orders.php?del_id=' . $order['order_id'] . '" class="btn btn-danger btn-sm">Delete</a></td>';
                                echo '</tr>';
                            }


                            ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</body>

</html>