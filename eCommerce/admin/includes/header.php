<header>
  <nav class="navbar navbar-expand-lg navbar-light bg-white border border-light shadow">
    <div class="navbar-brand font-weight-bold">
      <img src="../images/logo.jpg" width="125" title="Logo">
    </div>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarNavAltMarkup" style="justify-content:flex-end">
      <div class="navbar-nav" style="font-size:larger">
        <a class="nav-item nav-link" href="index.php">Admin Panel</a>
        <a class="nav-item nav-link" href="orders.php">Orders</a>
        <a class="nav-item nav-link" href="models.php">Models</a>
        <a class="nav-item nav-link" href="user/logout.php">Logout</a>
      </div>
    </div>
  </nav>
</header>