<?php

session_start();
if (!isset($_SESSION['user_email'])) {
    header('Location: ../index.php');
}

include 'includes/db.php';

//code for deleting the model
if (isset($_GET['del_id'])) {
    $del_id = $_GET['del_id'];

    $sel_model = "DELETE FROM models WHERE m_id = '$del_id'";
    if ($run_model_sql = mysqli_query($conn, $sel_model)) {
        header('Location: models.php?delRes=success');
    } else {
        header('Location: models.php?delRes=failure');
    }
}
if (isset($_GET['delRes'])) {
    if ($_GET['delRes'] == 'success') {
        $delResult = '<div class="alert alert-info">Order has been deleted!</div>';
    } else if ($_GET['delRes'] == 'failure') {
        $delResult = '<div class="alert alert-danger">Order could not be deleted!</div>';
    }
} else {
    $delResult = '';
}



if (isset($_GET['editRes'])) {
    if ($_GET['editRes'] == 'success') {
        $editResult = '<div class="alert alert-info">Model has been updated!</div>';
    } else if ($_GET['editRes'] == 'failure') {
        $editResult = '<div class="alert alert-danger">Model could not be updated!</div>';
    } else {
        $editResult = '<div class="alert alert-danger">Something went wrong!</div>';
    }
} else {
    $editResult = '';
}

if (isset($_GET['addRes'])) {
    if ($_GET['addRes'] == 'success') {
        $addResult = '<div class="alert alert-info">Model has been added!</div>';
    } else if ($_GET['addRes'] == 'failure') {
        $addResult = '<div class="alert alert-danger">Model could not be added!</div>';
    } else {
        $addResult = '<div class="alert alert-danger">Something went wrong!</div>';
    }
} else {
    $addResult = '';
}



?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Models</title>

    <!-- JQUERY LINKING HERE -->
    <script src="https://code.jquery.com/jquery-3.5.1.js" integrity="sha256-QWo7LDvxbWT2tbbQ97B53yJnYU3WhH/C8ycbRAkjPDc=" crossorigin="anonymous"></script>

    <!-- BOOTSTRAP CSS LINKING HERE -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">

    <!-- BOOTSTRAP JS LINKING HERE -->
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js" integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" crossorigin="anonymous"></script>

</head>

<body>
    <?php include 'includes/header.php'; ?>
    <!-- BUTTON SECTION -->
    <div class="my-3" style="display:flex;justify-content:flex-end;width:90%;">
        <a href="admin_panel.php" class="btn btn-info mx-5 col-md-3 shadow-lg">Go Back</a>
    </div>
    <div class="container my-3">
        <div><?php echo $delResult; ?></div>
        <div><?php echo $editResult; ?></div>
        <div class="card border border-info shadow-lg">
            <div class="card-header bg-white">
                <h2>Models</h2>
            </div>
            <div class="card-body">
                <table class="table">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Name</th>
                            <th colspan="2">Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $sel_models = "SELECT * FROM models";
                        $run_models_sql = mysqli_query($conn, $sel_models);
                        $count = 1;

                        while ($model = mysqli_fetch_assoc($run_models_sql)) {
                            echo '
                                <tr>
                                    <td>' . $count++ . '</td>
                                    <td>' . $model['m_name'] . '</td>
                                    <td><a href="edit_model.php?edit_id=' . $model['m_id'] . '" class="btn btn-info btn-sm">Edit</a></td>
                                    <td><a href="models.php?del_id=' . $model['m_id'] . '" class="btn btn-danger btn-sm">Delete</a></td>
                                </tr>
                            ';
                        }

                        ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</body>

</html>