<?php


session_start();

if(isset($_SESSION['user_email'])) {
    header('Location: admin_panel.php');
} else {
    header('Location: user/login.php');
}
