<?php

session_start();

if (isset($_SESSION['user_email'])) {
    header('Location: ../index.php');
}

if (isset($_GET['logout'])) {
    if ($_GET['logout'] == 'true') {
        $logoutRes = "true";
    } else {
        $logoutRes = "false";
    }
} else {
    $logoutRes = "false";
}

?>


<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Login</title>

    <!-- JQUERY LINKING HERE -->
    <script src="https://code.jquery.com/jquery-3.5.1.js" integrity="sha256-QWo7LDvxbWT2tbbQ97B53yJnYU3WhH/C8ycbRAkjPDc=" crossorigin="anonymous"></script>

    <!-- BOOTSTRAP CSS LINKING HERE -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">

    <!-- BOOTSTRAP JS LINKING HERE -->
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js" integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" crossorigin="anonymous"></script>

    <script>
        $(document).ready(function() {

            //LogOut Modal
            var logoutRes = <?php echo $logoutRes; ?>;
            if(logoutRes == true) $("#logout").modal();

            
            $("#submit").click(function() {

                var emailVal = $("#email").val();
                var passwordVal = $("#password").val();
                var valid = true;

                if (emailVal == '' || passwordVal == '') {
                    valid = false;
                } else {
                    switch (emailVal.indexOf('@')) {
                        case -1:
                            valid = false;
                            break;
                        case emailVal.length - 1:
                            valid = false;
                            break;
                        default:
                            console.log('Email Valid');
                            //other validations for the email
                            break;
                    }

                    if (passwordVal.length < 8) {
                        valid = false;
                    } else {
                        console.log('Password Valid');
                        //other validation for the password
                    }
                }

                if (valid) {
                    //send the ajax request
                    console.log('Sending Ajax Request');

                    $.ajax({
                        url: 'login_server_side.php',
                        method: 'post',
                        data: {
                            'email': emailVal,
                            'password': passwordVal
                        },
                        success: function(response) {
                            ///when the request succeeds
                            console.log(response);
                            switch (response) {
                                case 'success': {
                                    window.location.replace('../index.php');
                                    break;
                                }
                                case 'wrong_password': {
                                    alert('Wrong Password');
                                    break;
                                }
                                case 'not_found': {
                                    alert('No Such User Exists');
                                    break;
                                }
                                default: {
                                    alert('Something went wrong!');
                                    break;
                                }
                            }
                        },
                        error: function(jqXHR, textStatus, errorThrown) {
                            ///when the request fails
                            console.log(textStatus, errorThrown);
                        }
                    }).fail(function() {
                        console.log("FAILED TO SEND A REQUEST TO THE SERVER!");
                    });

                } else {
                    //some credentials were invlaid
                    alert('Enter valid credentials');
                }

            });


        });
    </script>
</head>

<body>
    <!-- LOGOUT MODAL -->
    <div class="modal fade border border-danger" id="logout" role="dialog">
        <div class="modal-dialog modal-dialog-centered">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <h5>You have been successfully logged out.</h5>
                    <button type="button" class="close text-danger" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            </div>
        </div>
    </div>

    <header>
        <nav class="navbar navbar-expand-lg navbar-light bg-white">
            <div class="navbar-nav" style="font-size:larger">
                <a class="nav-item nav-link active text-info" href="/ecommerce/ecommerce/">
                    <h5>⇦ Go Back</h5>
                </a>
            </div>
        </nav>
    </header>
    <div class="container">
        <div style="display:flex;justify-content:center;align-items:center;height: 80vh;">
            <div class="card border border-info" style="width: 80%;">
                <div class="card-header bg-white">
                    <h3>Enter Login Credentials</h3>
                </div>
                <div class="card-body">
                    <form action="#" class="form-horizontal" role="form">
                        <div class="form-group row">
                            <label for="email" class="col-md-2 text-center">Email: </label>
                            <div class="col-md-9"><input id="email" type="email" name="email" class="form-control" placeholder="Email" /></div>
                        </div>
                        <div class="form-group row">
                            <label for="password" class="col-md-2 text-center">Password: </label>
                            <div class="col-md-9"><input id="password" name="password" type="password" class="form-control" placeholder="Password" /></div>
                        </div>
                        <div>
                            <label class="col-md-2"></label>
                            <label><span style="font-size:small;">Note: </span><span style="font-size: smaller;">Password must contain at least 8 characters.</span></label>
                        </div>
                        <div class="form-group row">
                            <label for="password" class="col-md-2 text-center"></label>
                            <div class="col-md-9 text-center"><input id="submit" name="submit" type="button" class="btn col-md-4 btn-info" value="Submit" /></div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</body>

</html>