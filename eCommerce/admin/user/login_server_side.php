<?php


include '../includes/db.php';

session_start();

if (isset($_POST['email']) && isset($_POST['password'])) {
    $email = $_POST['email'];
    $password = $_POST['password'];
    $exists = false;

    $sel_user = "SELECT * FROM users";
    $run_user_sql = mysqli_query($conn, $sel_user);

    while ($user = mysqli_fetch_assoc($run_user_sql)) {
        if ($user['email'] === $email) {
            $exists = true;
            if ($user['password'] === $password) {
                $_SESSION['user_email'] = $user['email'];
                echo "success";
            } else {
                echo "wrong_password";
                session_destroy();
            }
        }
    }

    if (!$exists) {
        echo 'not_found';
    } else {
    }
} else {
    echo "failure";
}
