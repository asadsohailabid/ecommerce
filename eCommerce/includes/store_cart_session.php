<?php


// starting session here
session_start();

if (!isset($_SESSION['cart'])) {
    $_SESSION['cart'] = array();
}

if (isset($_POST['productId']) && isset($_POST['color'])) {

    $productDetails = [$_POST['productId'], $_POST['color']];
    $found = false;

    if (isset($_SESSION['cart'])) {
        foreach ($_SESSION['cart'] as $cartItem) {
            if (in_array($_POST['productId'], $cartItem)) {
                $found = true;
                break;
            }
        }
        if (!$found) array_push($_SESSION['cart'], $productDetails);
    } else {
        $_SESSION['cart'] = array();
        array_push($_SESSION['cart'], $productDetails);
    }

    if (!$found) echo "SUCCESS";
    else echo "FAILURE";
} else {
    echo 'Unknown';
}

// session_destroy();