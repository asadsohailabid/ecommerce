<!-- NAVIGATION BAR HERE -->
<header class="navbar navbar-expand-lg navbar-light bg-white border border-light shadow-sm">
    <div class="container">
        <a href="index.php" class="navbar-brand font-weight-bold">
            <img src="images/logo.jpg" width="125" title="Logo">
        </a>
        <ul class="navbar-nav">
            <li class="nav-item"><a class="nav-link" href="cart.php">
                    <img src="bootstrap-icons/cart.svg" width="25" height="25" title="Cart" style="margin-right:10px;">
                </a></li>
        </ul>
    </div>
</header>
