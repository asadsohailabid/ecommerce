<?php

include 'db.php';

session_start();

if (isset($_POST['flag'])) {

    //getting form data from the form
    $customerData = $_POST['customer'];
    //pseudo parse customer data
    $name = $customerData['name'];
    $email = $customerData['email'];
    $contact = $customerData['contact'];
    $address = $customerData['address'];
    $postal_code = $customerData['postal_code'];
    $city = $customerData['city'];

    $productData = $_POST['products'];

    // $name = $_POST['name'];
    // $email = $_POST['email'];
    // $contact = $_POST['contact'];
    // $address = $_POST['address'];

    $ins_order = "INSERT INTO orders 
        (cust_name, cust_email, cust_contact, cust_address, cust_postal_code, cust_city)
        VALUES ('$name','$email','$contact','$address','$postal_code','$city')
    ";

    if (mysqli_query($conn, $ins_order)) {
        $success = true;
        $order_id = mysqli_insert_id($conn);
    } else {
        $success = false;
        $order_id = "0";
    }

    if ($success) {
        if (isset($_POST['products'])) {

            $productData = $_POST['products'];

            foreach ($productData as $product) {

                $model_id = $product[0];
                $color = $product[1];
                $quantity = $product[2];

                $ins_product = "INSERT INTO ordered_products
                    (order_id, model_id, color, quantity)
                    VALUES ('$order_id','$model_id','$color','$quantity')
                ";

                if (mysqli_query($conn, $ins_product)) {
                    $success = true;
                } else {
                    $success = false;
                }
            }
        }
    }

    if($success) {
        //destroy session
        session_destroy();
        echo "SUCCESS";
        //redirect to home page
        // header('Location: ../index.php?storeRes=success');
    } else {
        echo "FAILURE";
        //redirect to cart page with error message
        // header('Location: ../cart.php?storeRes=failure');
    }

}
