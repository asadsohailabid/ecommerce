<?php


session_start();

if (isset($_POST['id'])) {
    $productId =  $_POST['id'];

    //check the index where this value exists in the session
    $productIndex = -1;
    foreach ($_SESSION['cart'] as $index => $cartItem) {
        if (in_array($productId, $cartItem)) {
            $productIndex = $index;
            break;
        }
    }

    //splice that certain session index
    if ($productIndex > -1) {
        array_splice($_SESSION['cart'], $productIndex, 1);
        echo "SUCCESS";
    } else if($productIndex == -1 ) {
        echo "FAILURE";
    }
} else {
    echo 'UNKNOWN';
}
