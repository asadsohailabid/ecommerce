<?php

include 'includes/db.php';


$sel_products = "SELECT * FROM models";
$run_sql = mysqli_query($conn, $sel_products);

$num_of_products = mysqli_num_rows($run_sql);


if (isset($_GET['storeRes'])) {
    if ($_GET['storeRes'] == "success")
        $thankYou = "true";
} else {
    $thankYou = "false";
}

?>


<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>E-Commerce</title>

    <!-- JQUERY LINKING HERE -->
    <script src="https://code.jquery.com/jquery-3.5.1.js" integrity="sha256-QWo7LDvxbWT2tbbQ97B53yJnYU3WhH/C8ycbRAkjPDc=" crossorigin="anonymous"></script>

    <!-- BOOTSTRAP CSS LINKING HERE -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">

    <!-- BOOTSTRAP JS LINKING HERE -->
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js" integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" crossorigin="anonymous"></script>

    <script>
        $(document).ready(function() {

            console.log();

            var thankYouMsgChecker = <?php echo $thankYou; ?>;
            if (thankYouMsgChecker == true) {
                $("#thankYou").modal();
            }

            //Backend for the navigation purposes
            $("#huawei_btn").click(function() {
                $("#huawei_models").css('display', '');
                $("#samsung_models").css('display', 'none');
                $("#iphone_models").css('display', 'none');
                $("#xiaomi_models").css('display', 'none');
            });
            $("#samsung_btn").click(function() {
                $("#huawei_models").css('display', 'none');
                $("#samsung_models").css('display', '');
                $("#iphone_models").css('display', 'none');
                $("#xiaomi_models").css('display', 'none');
            });
            $("#iphone_btn").click(function() {
                $("#huawei_models").css('display', 'none');
                $("#samsung_models").css('display', 'none');
                $("#iphone_models").css('display', '');
                $("#xiaomi_models").css('display', 'none');
            });
            $("#xiaomi_btn").click(function() {
                $("#huawei_models").css('display', 'none');
                $("#samsung_models").css('display', 'none');
                $("#iphone_models").css('display', 'none');
                $("#xiaomi_models").css('display', '');
            });

            //Remaining Code Here
            var totalPorducts = <?php echo $num_of_products; ?>;
            var selectedProduct = "-1";
            var selectColor = "-1";

            $("button").click(function() {
                var elementId = this.id;
                if (elementId > 0 && elementId <= totalPorducts) {
                    $("#add_to_cart").attr('disabled', false);
                    selectedProduct = elementId;
                } else {
                    switch (elementId) {
                        case "redBtn":
                            $("#mobileImg").attr("src", "images/mobile/red.jpg");
                            selectColor = "red";
                            break;
                        case "blackBtn":
                            $("#mobileImg").attr("src", "images/mobile/black.jpg");
                            selectColor = "black";
                            break;
                        case "blueBtn":
                            $("#mobileImg").attr("src", "images/mobile/blue.jpg");
                            selectColor = "blue";
                            break;
                        case "goldBtn":
                            $("#mobileImg").attr("src", "images/mobile/gold.jpg");
                            selectColor = "gold";
                            break;
                        case "purpleBtn":
                            $("#mobileImg").attr("src", "images/mobile/purple.jpg");
                            selectColor = "purple";
                            break;
                        case "silverBtn":
                            $("#mobileImg").attr("src", "images/mobile/silver.jpg");
                            selectColor = "silver";
                            break;
                    }
                }
            });

            $("#add_to_cart").click(function() {

                if (selectedProduct == "-1") {
                    alert("Product has not been selected yet!");
                } else if (selectColor == "-1") {
                    alert("Color has not been selected yet!");
                } else {
                    $.ajax({
                        url: "includes/store_cart_session.php",
                        method: "POST",
                        data: {
                            productId: selectedProduct,
                            color: selectColor
                        },
                        success: function(response) {
                            if (response === "SUCCESS") {
                                $("#successModal").modal();
                                selectColor = "-1";
                            } else if(response === "FAILURE") {
                                alert("Product already exists!");
                            }
                        },
                        error: function(jqXHR, textStatus, errorThrown) {
                            console.log(textStatus, errorThrown);
                        }
                    }).fail(function() {
                        console.log("FAILED TO SEND A REQUEST TO THE SERVER!");
                    });
                }
            });
        });
    </script>
</head>

<body>
    <!-- NAVIGATION BAR HERE -->
    <?php include 'includes/header.php'; ?>

    <!-- Success Modal Section -->
    <div class="modal fade" id="successModal" role="dialog">
        <div class="modal-dialog modal-dialog-centered">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <a href="cart.php" class="col-md-5 btn btn-block btn-danger">CheckOut Cart</a>
                    <input type="button" class="col-md-5 btn border border-dark" value="Continue Shopping" data-dismiss="modal" />
                </div>
                <div class="modal-body">
                    <p>The Product has successfully been added to the cart.</p>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade border border-danger" id="thankYou" role="dialog">
        <div class="modal-dialog modal-dialog-centered">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <h5>Message</h5>
                    <button type="button" class="close text-danger" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <h5 class="text-danger">Thank You For Shopping Here...&hearts;</h5>
                    <p>Your order has been placed!</p>

                </div>
            </div>
        </div>
    </div>
    <!-- PAGE CONTENT HERE -->
    <div class="container">
        <div style="display:flex;justify-content:center;height:85vh;">
            <!-- A card to contain all the data displayed to the user -->
            <div class="card" style="width:100%;border:none;">
                <div class="card-body bg-white row">
                    <!-- Left Section -->
                    <div class="col-md-6">
                        <div class="border-bottom row">
                            <label class="col-md-1"></label>
                            <div class="col-md-10">
                                <img id="mobileImg" src="images/temp.JPG" width="100%" />
                            </div>
                            <label class="col-md-1"></label>
                        </div>
                        <div class="border-bottom">
                            <h3>Choose Color: </h3>
                            <div style="display:flex;justify-content:center;">
                                <button id="redBtn" class="bg-white" style="border: 1px solid lightgrey;">
                                    <img  src="images/mobile/redThumbnail.JPG" width="90%">
                                </button>
                                <button id="blackBtn" class="bg-white" style="border: 1px solid lightgrey;">
                                    <img  src="images/mobile/blackThumbnail.JPG" width="90%">
                                </button>
                                <button id="blueBtn" class=" bg-white" style="border: 1px solid lightgrey;">
                                    <img  src="images/mobile/blueThumbnail.JPG" width="90%">
                                </button>
                                <button id="goldBtn" class=" bg-white" style="border: 1px solid lightgrey;">
                                    <img  src="images/mobile/goldThumbnail.JPG" width="90%">
                                </button>
                                <button id="purpleBtn" class=" bg-white" style="border: 1px solid lightgrey;">
                                    <img  src="images/mobile/purpleThumbnail.JPG" width="90%">
                                </button>
                                <button id="silverBtn" class=" bg-white" style="border: 1px solid lightgrey;">
                                    <img  src="images/mobile/silverThumbnail.JPG" width="90%">
                                </button>
                            </div>
                        </div>
                    </div>
                    <!-- Right Section -->
                    <div class="col-md-6 border">
                        <!-- Description Section -->
                        <div class="row" style="padding:15px;">
                            <label>Mobile Cases for Huawei, iPhones, Samsung & Xiaomi</label>
                        </div>

                        <!-- Price Section -->
                        <div class="row" style="padding:15px;">
                            <h2>€ 7.00</h2>
                        </div>
                        <hr />

                        <!-- Navigation Section -->
                        <nav class="col-md-6 navbar navbar-expand-md navbar-light bg-white">
                            <div class="container-fluid">
                                <ul class="" style="display:flex;padding:0px;list-style-type:none">
                                    <?php
                                    $sel_cat = "SELECT * FROM categories";
                                    $run_cat_sql = mysqli_query($conn, $sel_cat);
                                    while ($category = mysqli_fetch_assoc($run_cat_sql)) {
                                        switch ($category['cat_id']) {
                                            case '1':
                                                $id = 'huawei_btn';
                                                break;
                                            case '2':
                                                $id = 'samsung_btn';
                                                break;
                                            case '3':
                                                $id = 'iphone_btn';
                                                break;
                                            case '4':
                                                $id = 'xiaomi_btn';
                                                break;
                                        }
                                        echo '
                                            <li class="text-center">
                                                <button id="' . $id . '" class="btn border border-dark text-center">
                                                    ' . $category['cat_name'] . '
                                                </button>
                                            </li>
                                        ';
                                    }
                                    ?>
                                </ul>
                            </div>
                        </nav>
                        <hr />

                        <!-- Models Section -->
                        <div class="row" style="padding:5px;">
                            <label class="col-md-1"></label>
                            <div class="models col-md-10">
                                <h5>Models:</h5>
                                <div id="huawei_models">
                                    <?php
                                    $sel_model = "SELECT * FROM models WHERE cat_id = '1'";
                                    $run_model_sql = mysqli_query($conn, $sel_model);
                                    while ($model = mysqli_fetch_assoc($run_model_sql)) {
                                        echo '
                                        <button id="' . $model['m_id'] . '" class="btn btn-sm border border-dark">' . $model['m_name'] . '</button>
                                    ';
                                    }
                                    ?>
                                </div>
                                <div id="samsung_models" style="display: none;">
                                    <?php
                                    $sel_model = "SELECT * FROM models WHERE cat_id = '2'";
                                    $run_model_sql = mysqli_query($conn, $sel_model);
                                    while ($model = mysqli_fetch_assoc($run_model_sql)) {
                                        echo '
                                    <button id="' . $model['m_id'] . '" class="btn btn-sm border border-dark">' . $model['m_name'] . '</button>
                                    ';
                                    }
                                    ?>
                                </div>
                                <div id="iphone_models" style="display: none;">
                                    <?php
                                    $sel_model = "SELECT * FROM models WHERE cat_id = '3'";
                                    $run_model_sql = mysqli_query($conn, $sel_model);
                                    while ($model = mysqli_fetch_assoc($run_model_sql)) {
                                        echo '
                                    <button id="' . $model['m_id'] . '" class="btn btn-sm border border-dark">' . $model['m_name'] . '</button>
                                    ';
                                    }
                                    ?>
                                </div>
                                <div id="xiaomi_models" style="display: none;">
                                    <?php
                                    $sel_model = "SELECT * FROM models WHERE cat_id = '4'";
                                    $run_model_sql = mysqli_query($conn, $sel_model);
                                    while ($model = mysqli_fetch_assoc($run_model_sql)) {
                                        echo '
                                    <button id="' . $model['m_id'] . '" class="btn btn-sm border border-dark">' . $model['m_name'] . '</button>
                                    ';
                                    }
                                    ?>
                                </div>
                            </div>
                        </div>
                        <hr />

                        <!-- Buttons Section -->
                        <div class="row">
                            <label class="col-md-1"></label>
                            <button type="button" class="col-md-10 btn btn-danger btn-lg btn-block" id="add_to_cart" disabled>Add To Cart</button>
                            <label class="col-md-1"></label>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- FOOTER HERE -->
    <footer>

    </footer>
</body>

</html>