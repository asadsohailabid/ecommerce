-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Nov 06, 2020 at 03:56 AM
-- Server version: 10.4.14-MariaDB
-- PHP Version: 7.4.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ecommerce`
--

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `cat_id` int(11) NOT NULL,
  `cat_name` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`cat_id`, `cat_name`) VALUES
(1, 'Huawei'),
(2, 'Samsung'),
(3, 'iPhone'),
(4, 'Xiaomi');

-- --------------------------------------------------------

--
-- Table structure for table `models`
--

CREATE TABLE `models` (
  `m_id` int(11) NOT NULL,
  `cat_id` int(11) NOT NULL,
  `m_name` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `models`
--

INSERT INTO `models` (`m_id`, `cat_id`, `m_name`) VALUES
(1, 1, 'Honor20-Lite'),
(2, 1, 'Honor8-X'),
(3, 1, 'Honor20'),
(4, 1, 'Honor20-Pro'),
(5, 1, 'Honor9-A'),
(6, 1, 'Honor9-C'),
(7, 1, 'Honor9-S'),
(8, 1, 'Honor-9X'),
(9, 1, 'Honor9-X-Lite'),
(10, 1, 'Mate30-Lite'),
(11, 1, 'Mate10-Lite'),
(12, 1, 'Mate20-Lite'),
(13, 1, 'Mate20-Pro'),
(14, 1, 'Nova5-T'),
(15, 1, 'Nova7'),
(16, 1, 'Nova7-SE'),
(17, 1, 'P-Smart-S'),
(18, 1, 'P-Smart(2019)'),
(19, 1, 'P20-Lite'),
(20, 1, 'P20-Pro'),
(21, 1, 'P30'),
(22, 1, 'P30-Lite'),
(23, 1, 'P30-Pro'),
(24, 1, 'P40'),
(25, 1, 'P4-Lite'),
(26, 1, 'P40-Pro'),
(27, 1, 'Y5(2018)'),
(28, 1, 'Y5(2019)'),
(29, 1, 'Y5_P'),
(30, 1, 'Y6(2018)'),
(31, 1, 'Y6(2019)'),
(32, 1, 'Y6-P'),
(33, 1, 'Y6-S'),
(34, 1, 'Y7(2018)'),
(35, 1, 'Y7(2019)'),
(36, 1, 'Y8-S'),
(37, 1, 'Y90-Prime(2019)'),
(38, 2, 'A6-Plus'),
(39, 2, 'A7(2018)'),
(40, 2, 'A70-A70S'),
(41, 2, 'A70-E'),
(42, 2, 'A8(2018)'),
(43, 2, 'A80-A90'),
(44, 2, 'A8-Plus'),
(45, 2, 'A9(2019)'),
(46, 2, 'J4(2018)'),
(47, 2, 'J4-Plus'),
(48, 2, 'J6(2018)'),
(49, 2, 'J6-Plus'),
(50, 2, 'J8'),
(51, 2, 'M11-A11'),
(52, 2, 'M20'),
(53, 2, 'M21-M30S-M31'),
(54, 2, 'M30'),
(55, 2, 'M51'),
(56, 2, 'Note10'),
(57, 2, 'Note10-Lite'),
(58, 2, 'Note10-Pro'),
(59, 2, 'Note8'),
(60, 2, 'Note9'),
(61, 2, 'S10'),
(62, 2, 'S10-E'),
(63, 2, 'S10-Plus'),
(64, 2, 'S7-Edge'),
(65, 2, 'S8'),
(66, 2, 'S8-Plus'),
(67, 2, 'S9'),
(68, 3, 'iPhone SE2020'),
(69, 3, 'iPhone X'),
(70, 3, 'iPhone XR'),
(71, 3, 'iPhone XS MAX'),
(72, 3, 'iPhone 11(5.8)'),
(73, 3, 'iPhone 7-8'),
(74, 3, 'iPhone 7Plus-8Plus'),
(75, 3, 'iPhone 11(6,1)'),
(76, 3, 'iPhone 11(6.5)'),
(77, 4, 'A3Lite - MI9Lite'),
(78, 4, 'MI 6X-A2'),
(79, 4, 'MI 8-Lite'),
(80, 4, 'MI 9T'),
(81, 4, 'MI A3'),
(82, 4, 'MI A3-Lite'),
(83, 4, 'MI Note10'),
(84, 4, 'MI Note10-Lite'),
(85, 4, 'MI Note10-Pro'),
(86, 4, 'MI8'),
(87, 4, 'MI 9-Pro'),
(88, 4, 'Note9S - Note9Pro'),
(89, 4, 'Redmi 5'),
(90, 4, 'Redmi 5-Plus'),
(91, 4, 'Redmi 6A'),
(92, 4, 'Redmi 6-Pro'),
(93, 4, 'Redmi 7'),
(94, 4, 'Redmi 8A'),
(95, 4, 'Redmi 9'),
(96, 4, 'Redmi 9A'),
(97, 4, 'Redmi Note4X'),
(98, 4, 'Redmi Note5-Pro'),
(99, 4, 'Redmi Note5A'),
(100, 4, 'Redmi Note6-Pro'),
(101, 4, 'Redmi Note7'),
(102, 4, 'Redmi Note8'),
(103, 4, 'Redmi Note8-Pro'),
(104, 4, 'Redmi Note8T'),
(105, 4, 'Redmi Note9'),
(106, 4, 'Redmi S2'),
(107, 4, 'Redmi 6'),
(108, 4, 'Redmi 8');

-- --------------------------------------------------------

--
-- Table structure for table `ordered_products`
--

CREATE TABLE `ordered_products` (
  `op_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `model_id` int(11) NOT NULL,
  `color` text NOT NULL,
  `quantity` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `ordered_products`
--

INSERT INTO `ordered_products` (`op_id`, `order_id`, `model_id`, `color`, `quantity`) VALUES
(14, 8, 74, 'black', 20);

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `order_id` int(11) NOT NULL,
  `cust_name` text NOT NULL,
  `cust_email` text NOT NULL,
  `cust_contact` text NOT NULL,
  `cust_address` text NOT NULL,
  `cust_postal_code` int(11) NOT NULL,
  `cust_city` text NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`order_id`, `cust_name`, `cust_email`, `cust_contact`, `cust_address`, `cust_postal_code`, `cust_city`, `status`) VALUES
(8, 'Asad Sohail', 'asadsohailabid@gmail.com', '12345678', 'Baker Street', 2000, 'London', 1);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `user_id` int(11) NOT NULL,
  `email` text NOT NULL,
  `password` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`user_id`, `email`, `password`) VALUES
(1, 'asad@gmail.com', 'asad1234');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`cat_id`);

--
-- Indexes for table `models`
--
ALTER TABLE `models`
  ADD PRIMARY KEY (`m_id`);

--
-- Indexes for table `ordered_products`
--
ALTER TABLE `ordered_products`
  ADD PRIMARY KEY (`op_id`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`order_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `cat_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `models`
--
ALTER TABLE `models`
  MODIFY `m_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=110;

--
-- AUTO_INCREMENT for table `ordered_products`
--
ALTER TABLE `ordered_products`
  MODIFY `op_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `order_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
